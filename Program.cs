﻿
const int KEO = 1;
const int BUA = 2;
const int BAO = 3;


string UserChoice(int nguoiChoi1, int nguoiChoi2)
{
    if (nguoiChoi1 == nguoiChoi2)
    {
        return "Hoa nhau";
    }
    else if ((nguoiChoi1 == 1 && nguoiChoi2 == 3) ||
            (nguoiChoi1 == 2 && nguoiChoi2 == 1) ||
            (nguoiChoi1 == 3 && nguoiChoi2 == 2))
    {
        return "Nguoi choi 1 thang";
    }
    else
    {
        return "Nguoi choi 2 thang";
    }
}

void Menu()
{
    Console.WriteLine("----KEO BUA BAO----");
    Console.WriteLine("1. Choi");
    Console.WriteLine("2. Thoat");
    Console.WriteLine("-------------------");
    Console.WriteLine("Nhap lua chon: ");
}

int choice = 0;
do
{
    Menu();
    choice = int.Parse(Console.ReadLine());
    Console.Clear();

    switch (choice)
    {
        case 1:
            Console.WriteLine("1. KEO");
            Console.WriteLine("2. BUA");
            Console.WriteLine("3. BAO");
            int nguoi1Chon = 0;
            int nguoi2Chon = 0;
            Console.WriteLine("Nguoi choi 1: ");
            nguoi1Chon = int.Parse(Console.ReadLine());
            Console.WriteLine("Nguoi choi 2: ");
            nguoi2Chon = int.Parse(Console.ReadLine());
            string ketQua = UserChoice(nguoi1Chon, nguoi2Chon);
            Console.WriteLine(ketQua);
            break;
        case 2:
            Console.WriteLine("Hen gap lai");
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Nhap cac lua chon tren bang");
            break;
    }
} while (choice != 2);